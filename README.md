# SystemRescue Infrastructure as Code

## Overview
The code in this repository is used to manage the infrastructure of the
SystemRescue project.

## Ansible

### Architecture
Servers are configured using Ansible. Each playbook applies a single top-level
role. A top-level roles correspond to a particular type of server, such as
"web server" or "git server". The names of top-level roles start with the
prefix "main-". Each top-level role includes multiple ordinary roles. Each
ordinary role provides support for a particular aspect of the system, such
as "iptables", "nginx", or "system user accounts". Each playbook uses a
single top-level role such as "main-websrv". Please refer to "the roles and
profiles method" as part of the puppet documentation to get more information
about this way of designing roles.

### Role variables
The code of the roles should remain generic and parameterized using variables
named after the role. For example all variables used by the the role for
"iptables" should be prefixed with "iptables_" so it is clear which role each
variable belongs to. This way the variables for the role belongs to a particular
namespace which is isolated from other roles. It should be possible to reuse
each role independently of each other. You can then use variable definitions
in group_vars and host_vars to override role variables and you can introduce
additional variables in these levels if necessary.

Specific details such as user names and addresses should be defined in group_vars
so the default variables definitions in the role remain generic.

## Secrets
The code in this repository depends on some sensitive information, which is
stored in a git-crypt encrpyted repository which is accessed as a git submodule.
