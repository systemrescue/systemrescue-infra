---

# fully reinstall the VM with Rocky Linux, controlled by a kickstart file
#
# only partially idempotent:
# - comparing current state to what is described in a kickstart is complex
# - a full resinstall is quite aggressive, so we don't want to do it accidently
# 
# decision if to reinstall or not is done like this:
# - /root/original-ks.cfg is compared to the output of the template
#   when the files differ, a reinstall is planned
# - if a reinstall is planned, but the file /FORMAT_EVERYTHING does not exist,
#   the play is aborted with an error. This acts as a safeguard agains accidental
#   reinstall
#
# Reinstall is done by downloading network boot kernel & initrd of Rocky Linux
# Then use kexec to boot these.
# The kickstart file is written to the /boot partition beforehand, anaconda
# is then instructed to load it from there. This requires /boot to be a separate
# partition that is not on LVM or RAID

- name: do kickstart install when necessary
  block:
  
    - name: original-ks.cfg existing?
      stat:
        path: /root/original-ks.cfg
      register: original_ks

    - name: Check kickstart template
      template:
        src: ks.cfg.j2
        dest: /root/original-ks.cfg
        backup: true
      register: kickstartfile

    - name: Safeguard - Check that we really want to reinstall
      stat:
        path: /FORMAT_EVERYTHING
      register: format_everything
      when: kickstartfile.changed
      failed_when: not format_everything.stat.exists
    
    - name: do reinstall when kickstart file changed and safeguard removed
      import_tasks: reinstall.yml
      when: kickstartfile.changed and format_everything.stat.exists

  rescue:
    - name: restore original kickstart file in case safeguard is in place
      copy:
         remote_src: true
         dest: /root/original-ks.cfg
         src: "{{ kickstartfile['backup_file'] }}"
      when: original_ks.stat.exists

    - name: remove backup kickstart file
      file:
         path: "{{ kickstartfile['backup_file'] }}"
         state: absent
      when: original_ks.stat.exists

    - name: remove kickstart file when it was not existing before
      file:
         path:  /root/original-ks.cfg
         state: absent
      when: not original_ks.stat.exists

