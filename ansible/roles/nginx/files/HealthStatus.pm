#!/usr/bin/perl
# 
# nginx Embedded Perl module for checking a health status file
# returns the result as HTTP status code: 200 OK or 500 for not OK
#
# file to read has to be configured like this before activating the module:
#
#   set $status_file "/var/lib/nginx/some-dir/status.json";
#   perl HealthStatus::handler;
#

package HealthStatus;

use nginx;
use JSON;         # requires packages perl-JSON, perl-JSON-PP

sub handler {

    my $r = shift;
    
    # never cache this result for long
    $r->header_out( "Surrogate-Control", "max-age=30" );
    
    open my $fh, '<', $r->variable("status_file");
    if ($@)
    {
        $r->status(500);
        $r->send_http_header("text/plain");
        $r->print("statusfile not found");
        return OK;
    }
    
    read $fh, my $file_content, -s $fh;
    close $fh;

    $json = JSON->new->allow_nonref;
    my $status_data = eval { decode_json($file_content) };
    if ($@)
    {
        $r->status(500);
        $r->send_http_header("text/plain");
        $r->print("statusfile not json");
        return OK;
    }

    if (! exists $$status_data{'status'} || ! exists $$status_data{'timestamp'})
    {
        $r->status(500);
        $r->send_http_header("text/plain");
        $r->print("statusfile incomplete");
        return OK;
    }

    if ($$status_data{'status'} ne "UP"
        || $$status_data{'timestamp'} !~ /^-?\d+$/
        || $$status_data{'timestamp'} < time() - 120 )
    {
        $r->status(500);
        $r->send_http_header("text/plain");
        $r->print("DOWN");
        return OK;
    }

    $r->status(200);
    $r->send_http_header("text/plain");
    $r->print("UP");
    return OK;
}

1;
__END__
